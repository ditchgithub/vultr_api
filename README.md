# vultr_api

description :

quickly deploy/destroy VPS in vultr with the command line

syntax :
```
$ vultr 

Usage: vultr_api.py [OPTION]

        -lcs    --list-current-servers
        -lo     --list-os
        -lr     --list-regions
        -lp     --list-plans
        -di     --deploy-interactive
        -Di     --Destroy-interactive
```

*** -  this repository is mirrored from gitea ***
